﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fenreliania.StateMachine
{
	public delegate bool StateChangeTrigger(int _eventId);

	public class StateMachine
	{
		public State InitialState;
		public State CurrentState;

		public StateMachine(State _initialState)
		{
			InitialState = _initialState;
			CurrentState = _initialState;
		}

		public void Reset()
		{
			CurrentState = InitialState;
		}

		public void TriggerStateChange(int _eventId)
		{
			foreach (KeyValuePair<StateChangeTrigger, State> trigger in CurrentState.Triggers)
			{
				if (trigger.Key(_eventId))
				{
					CurrentState = trigger.Value;
					TriggerStateChange(-1);
					return;
				}
			}
		}
	}

	public class State
	{
		public Dictionary<StateChangeTrigger, State> Triggers;
	}
}
